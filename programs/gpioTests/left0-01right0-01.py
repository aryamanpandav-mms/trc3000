import RPi.GPIO as GPIO
import time 

# Constants --------------------------------------

MOTOR_1_DIRECTION = 17
MOTOR_1_SPEED_BIT_1 = 27
MOTOR_1_SPEED_BIT_2 = 22

MOTOR_2_DIRECTION = 10
MOTOR_2_SPEED_BIT_1 = 9
MOTOR_2_SPEED_BIT_2 = 11


# Code -------------------------------------------

outputLowList = [MOTOR_1_DIRECTION, MOTOR_2_DIRECTION, MOTOR_1_SPEED_BIT_1, MOTOR_2_SPEED_BIT_1]
outputHighList = [MOTOR_1_SPEED_BIT_2, MOTOR_2_SPEED_BIT_2]

GPIO.setmode(GPIO.BCM)
GPIO.setup(outputLowList, GPIO.OUT)
GPIO.setup(outputHighList, GPIO.OUT)

while(1):
	GPIO.output(outputLowList, 0) 
	GPIO.output(outputHighList, 1)
